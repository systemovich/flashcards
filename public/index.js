hoodie.store.add([
    {
        front: 'Enter directory with path /usr/bin',
        back: 'cd /usr/bin',
    },
    {
        front: 'Show all files in current directory',
        back: 'ls',
    },
]).then(function (flashcards) {
    console.log(flashcards);
    let pack = document.querySelector('.pack');
    flashcards.forEach(function (flashcard) {
        let cardContainer = document.createElement('div');
        cardContainer.innerHTML = '<div>'
            + '<strong>front:</strong> ' + flashcard.front
            + '<strong>|</strong> ' 
            + '<strong>back:</strong> ' + flashcard.back 
            + '</div>';
        pack.appendChild(cardContainer);
    });
});
